This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# screen-scraper-fe
>  Frontend app that takes an URL input and displays a screenshot of that page. Refer to [this project](https://gitlab.com/klemen.kunstek1/screen-scrapper-be) for backend.

## Getting Started

In the project directory, you can run:

### `npm install`

Installs dependencies.<br>

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
