import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "https://google.com",
      image: "",
      isLoading: false,
      error: null
    };
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>Screen scrapper</p>
        </header>
        <div className="App-body">
          <input
            type="text"
            name="url"
            value={this.state.url}
            onChange={this.handleUrlChange}
          />
          <br />
          <button onClick={this.handleSubmit}>GO!</button>
          <br />
          {this.state.isLoading && <p className="App-loading">loading ...</p>}
          {this.state.error && <code>{this.state.error}</code>}
          {this.state.image && <img src={`${this.state.image}`} />}
        </div>
      </div>
    );
  }

  // update input value
  handleUrlChange = e => {
    this.setState({ url: e.target.value });
  };

  handleSubmit = async () => {
    // check if url has http or https
    if (!this.isInputValid()) return;

    try {
      // stop loader, clear image
      this.setState({ image: null, error: null, isLoading: true });

      // call backend
      const res = await this.getScreenshot(this.state.url);
      console.log(res);
      const { status } = res;
      const resBody = await res.json();

      // handle backend response
      if (status >= 200 && status < 300) {
        this.setState({ image: resBody.img, isLoading: false });
      } else {
        this.setState({ error: resBody.error, isLoading: false });
      }
    } catch (e) {
      this.setState({ error: e, isLoading: false });
      console.error();
    }
  };

  isInputValid = () => {
    const urlRegex = "^(http|https)://[a-zA-Z0-9-._?,'/\\+&amp;%$#=~]+$";
    if (!this.state.url.match(new RegExp(urlRegex))) {
      this.setState({
        image: null,
        error: "Insert valid url with http:// or https://"
      });
      return false;
    }
    return true;
  };

  // backend call
  getScreenshot = screenshotPageUrl => {
    // this assumes your local backend is running at port 4000
    const REQUEST_URL = `http://localhost:4000/screenshot?url=${screenshotPageUrl}`;

    return fetch(`${REQUEST_URL}`, {
      headers: {
        "Content-Type": "application/json"
      },
      method: "GET"
    });
  };
}

export default App;
